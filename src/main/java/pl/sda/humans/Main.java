package pl.sda.humans;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {

        Gender male = Gender.MALE;
        System.out.println(male.getPrintableName());
        System.out.println(Gender.FEMALE.getPrintableName());

        List<Human> humans = new LinkedList<>();
        humans.add(new Human("Adam", 12, Gender.MALE, BigDecimal.TEN));
        humans.add(new Human("Basia", 30, Gender.FEMALE, BigDecimal.TEN));
        humans.add(new Human("Paweł", 20, Gender.MALE, BigDecimal.TEN));
        humans.add(new Human("Kasia", 25, Gender.FEMALE, BigDecimal.TEN));
        humans.add(new Human("Marcin", 101, Gender.MALE, BigDecimal.TEN));


        //4 == 2+2
        //2+2 == 4
        System.out.println("Mezczyzni:");
        for (Human human : humans) {
            if (Gender.MALE.equals(human.getGender())) {
                System.out.println(human);
            }
        }
        System.out.println("Mezczyzni stream:");
        humans.stream()
                .filter(human -> Gender.MALE.equals(human.getGender()))
                .forEach(human -> System.out.println(human));
        System.out.println("Kobiety:");
        for (Human human : humans) {
            if (Gender.FEMALE.equals(human.getGender())) {
                System.out.println(human);
            }
        }
        System.out.println("Kobiety stream:");
        humans.stream()
                .filter(human -> Gender.FEMALE.equals(human.getGender()))
                .forEach(human -> System.out.println(human));

        System.out.println("Osoby pelnoletnie:");
        for (Human human : humans) {
            if (human.getAge() >= 18) {
                System.out.println(human);
            }
        }
        System.out.println("Osoby pelnoletnie stream");
        humans.stream()
                .filter(human -> human.getAge() >= 18)
                .forEach(human -> System.out.println(human));
        System.out.println("Wielokrotnosc 10:");
        for (Human human : humans) {
            if (human.getAge() % 10 == 0) {
                System.out.println(human);
            }
        }
        System.out.println("Wielokrotnosc 10 stream:");
        humans.stream()
                .filter(human -> human.getAge() % 10 == 0)
                .forEach(human -> System.out.println(human));
        System.out.println("Wiek wszystkich osób:");
        int sumOfAge = 0;
        for (Human human : humans) {
            sumOfAge += human.getAge();
        }
        System.out.println(sumOfAge);
        System.out.println("Wiek wszystkich osób stream:");

        int sumOfAgeStream = humans.stream()
                .mapToInt(human -> human.getAge())
                .sum();

        System.out.println(sumOfAgeStream);

        System.out.println("Wszystkie osoby mające \"si\" w imieniu:");
        for (Human human : humans) {
            if (human.getName().contains("si")) {
                System.out.println(human);
            }
        }
        System.out.println("Wszystkie osoby mające \"si\" w imieniu stream:");
        humans.stream()
                .filter(human -> human.getName().contains("si"))
                .forEach(System.out::println);

        System.out.println("Sumowanie pieniedzy:");

        BigDecimal sumOfMoney = BigDecimal.ZERO;
        for (Human human : humans) {
            sumOfMoney = sumOfMoney.add(human.getMoney());
        }
        System.out.println(sumOfMoney);

        System.out.println("Sumowanie pieniedzy stream:");

        BigDecimal streamSumOfMoney = humans.stream()
                .map(human -> human.getMoney())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(streamSumOfMoney);
        /*
        //HOW TO USE BIG DECIMALS
        BigDecimal myBigDecimal = new BigDecimal("11");
        BigDecimal sumOfNumbers = myBigDecimal.add(new BigDecimal(5));
        System.out.println(sumOfNumbers);
        */
    }
}
