package pl.sda.humans;

import java.math.BigDecimal;

public class Human {
    private String name;
    private int age;
    private Gender gender;
    private BigDecimal money;

    public Human(String name, int age, Gender gender, BigDecimal money) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.money = money;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Imie: " + name + " Wiek: " + age + " Płeć: " + gender.getPrintableName();
    }
}
