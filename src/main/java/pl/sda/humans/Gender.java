package pl.sda.humans;

public enum Gender {
    MALE("Mężczyzna"), FEMALE("Kobieta");

    private final String printableName;

    Gender(String printableName) {
        this.printableName = printableName;
    }

    public String getPrintableName() {
        return printableName;
    }
}
